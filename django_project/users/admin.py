from django.contrib import admin
from .models import Profile


# Register your models here.

'''
Регистрация моделей для отображения их в админке.

'''

admin.site.register(Profile)

