from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm


def register(request):
    if request.method == 'POST':
        # form = UserCreationForm(request.POST)
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()  # сохранениею формы регистрации после прохождения валидации.
            username = form.cleaned_data.get('username')
            # messages.success(request, f'Account created for {username}!')
            messages.success(request, f' Your account has been created!  You are now able to log in.')
            # return redirect('blog_home')
            return redirect('login')
    else:
        # form = UserCreationForm()
        form = UserRegisterForm()

    return render(request, 'users/register.html', context={'form': form})
    # return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f' Your account has been updated!')
            return redirect('profile')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'users/profile.html', context=context)



'''
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
form.save() - сохранениею формы регистрации после прохождения валидации.
------------------------------------------------------------------------------------------------------------------------
username = form.cleaned_data.get('username') получение данных из поля username что, был отправлен из формы регистрации 
            нового пользователя в запросе POST и прошли валидацию методом is_valid() 
------------------------------------------------------------------------------------------------------------------------
https://djbook.ru/rel1.9/ref/contrib/messages.html

messages.debug() Сообщения, связанные с разработкой, которые будут игнорироваться (или удаляться) в производственном развертывании.
messages.info() Информационные сообщения для пользователя.
messages.success() Действие было успешным, например, «Ваш профиль был успешно обновлен».
messages.warning() Ошибка не произошла, но может быть неизбежной.
messages.error() Действие не было успешным или произошла какая-то другая ошибка.

------------------------------------------------------------------------------------------------------------------------
Form.is_valid()

Главной задачей объекта Form является проверка данных. У заполненного экземпляра Form вызовите метод is_valid() 
для выполнения проверки и получения её результата:

>>> data = {'subject': 'hello',
...         'message': 'Hi there',
...         'sender': 'foo@example.com',
...         'cc_myself': True}
>>> f = ContactForm(data)
>>> f.is_valid()
True

Начнём с неправильных данных. В этом случае поле subject будет пустым (ошибка, так как по умолчанию все поля должны 
быть заполнены), а поле sender содержит неправильный адрес электронной почты:

>>> data = {'subject': '',
...         'message': 'Hi there',
...         'sender': 'invalid email address',
...         'cc_myself': True}
>>> f = ContactForm(data)
>>> f.is_valid()
False

Form.errors
Обратитесь к атрибуту errors для получения словаря с сообщениями об ошибках:

https://djbook.ru/rel1.9/ref/forms/api.html

------------------------------------------------------------------------------------------------------------------------

Form.cleaned_data

Доступ к “чистым” данным

Каждое поле в классе Form отвечает не только за проверку, но и за нормализацию данных. Это приятная особенность, 
так как она позволяет вводить данные в определённые поля различными способами, всегда получая правильный результат.

Например, класс DateField нормализует введённое значение к объекту datetime.date. Независимо от того, передали ли вы 
строку в формате '1994-07-15', объект datetime.date или число в других форматах, DateField всегда преобразует его в 
объект datetime.date, если при этом не произойдёт ошибка.

После создания экземпляра Form, привязки данных и их проверки, вы можете обращаться к “чистым” данным через атрибут 
cleaned_data:

>>> data = {'subject': 'hello',
...         'message': 'Hi there',
...         'sender': 'foo@example.com',
...         'cc_myself': True}
>>> f = ContactForm(data)
>>> f.is_valid()
True
>>> f.cleaned_data
{'cc_myself': True, 'message': 'Hi there', 'sender': 'foo@example.com', 'subject': 'hello'}

Следует отметить, что любое текстовое поле, такое как CharField или EmailField, всегда преобразует текст в юникодную 
строку. Мы рассмотрим применения кодировок далее.

Если данные не прошли проверку, то атрибут cleaned_data будет содержать только значения тех полей, что прошли проверку:

>>> data = {'subject': '',
...         'message': 'Hi there',
...         'sender': 'invalid email address',
...         'cc_myself': True}
>>> f = ContactForm(data)
>>> f.is_valid()
False
>>> f.cleaned_data
{'cc_myself': True, 'message': 'Hi there'}

Атрибут cleaned_data всегда содержит только данные для полей, определённых в классе Form, даже если вы передали 
дополнительные данные при определении Form. В этом примере, мы передаём набор дополнительных полей в конструктор 
ContactForm, но cleaned_data содержит только поля формы:

>>> data = {'subject': 'hello',
...         'message': 'Hi there',
...         'sender': 'foo@example.com',
...         'cc_myself': True,
...         'extra_field_1': 'foo',
...         'extra_field_2': 'bar',
...         'extra_field_3': 'baz'}
>>> f = ContactForm(data)
>>> f.is_valid()
True
>>> f.cleaned_data # Doesn't contain extra_field_1, etc.
{'cc_myself': True, 'message': 'Hi there', 'sender': 'foo@example.com', 'subject': 'hello'}

Если Form прошла проверку, то cleaned_data будет содержать ключ и значение для всех полей формы, даже если данные не 
включают в себя значение для некоторых необязательных полей. В данном примере, словарь данных не содержит значение для 
поля nick_name, но cleaned_data содержит пустое значение для него:

>>> from django.forms import Form
>>> class OptionalPersonForm(Form):
...     first_name = CharField()
...     last_name = CharField()
...     nick_name = CharField(required=False)
>>> data = {'first_name': 'John', 'last_name': 'Lennon'}
>>> f = OptionalPersonForm(data)
>>> f.is_valid()
True
>>> f.cleaned_data
{'nick_name': '', 'first_name': 'John', 'last_name': 'Lennon'}

В приведённом выше примере, значением атрибута cleaned_data для поля nick_name является пустая строка, так как 
nick_name – это CharField, а CharField рассматривает пустые значения как пустые строки. Каждый тип поля знает, что 
такое “пустое” значение, т.е. для DateField – это None, на не пустая строка. Для получения подробностей о поведении 
каждого типа поля обращайте внимание на заметку “Пустое значение” для каждого поля в разделе “Встроенные поля”, которые 
приведён далее.

Вы можете написать код для выполнения проверки определённых полей формы (используя их имена) или для проверки всей 
формы (рассматривая её как комбинацию полей). Подробная информация изложена в Проверка форм и полей формы.

https://djbook.ru/rel1.9/ref/forms/api.html
------------------------------------------------------------------------------------------------------------------------

'''
