'''

В этом файле мы собираемся создать нашу первую форму, что наследуется от UserCreationForm
во-первых нам нужно сделать несколько импортов, нам нужно импортировать формы из джанго - from django import forms
и за тем на следующей линии нам нужно импортировать нашу user модель - from django.contrib.auth.models import User
и наконец мы сделаем импорт на user creation form - from django.contrib.auth.forms import UserCreationForm
и теперь мы создадим новую форму, что наследуется от UserCreationForm
я собираюсь создать новый класс, новую форму, что нследуется от UserCreationForm

'''

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']




'''
EmailField - class EmailField(max_length=254, **options)
Поле CharField для хранения правильного email-адреса. Использует EmailValidator для проверки значения.
'''





















