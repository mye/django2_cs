from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Post

from django.core.paginator import Paginator


# posts = [
#     {
#         'author': 'CoreyMS',
#         'title': 'Blog Post 1',
#         'content': 'First post content',
#         'date_posted': 'April 11, 2019'
#     },
#     {
#         'author': 'Jane Doe',
#         'title': 'Blog Post 2',
#         'content': 'Second post content',
#         'date_posted': 'April 12, 2019'
#     }
# ]

'''
Для использования данных из базы данных вместо "фиктивных данных" из списка posts:

    1. Импортировать модель Post из файла model.py - from .models import Post
    2. И подставить модель с выборкой всех постов из базы данных в аргумент content метода 
       render функции home - context={'posts': Post.objects.all()} вместо списка posts.
    
'''


# def home(request):
    # context = {
    #     'posts': posts
    # }
    # return render(request, 'blog/home.html', context=context)
    # return render(request, 'blog/home.html', context={'posts': posts})
    # return render(request, 'blog/home.html', context={'posts': Post.objects.all()})


class PostListView(ListView):
    model = Post

    # queryset = Post.objects.all()
    # queryset = Post.objects.filter(author_id=1)
    # queryset = Post.objects.order_by('-date_posted')
    # queryset = Post.objects.order_by('date_posted')
    # queryset = Post.objects.order_by('author')
    # queryset = Post.objects.order_by('-author')

    template_name = 'blog/home.html'  # в какой шаблон будет рендерить. по умолчанию будет/<app>/<model>_<viewtype>.html
    context_object_name = 'posts'  # название списка который будет рендериться в шаблон для форка.
    ordering = ['-date_posted']
    paginate_by = 5


class UserPostListView(ListView):
    model = Post

    # queryset = Post.objects.all()
    # queryset = Post.objects.filter(author_id=1)
    # queryset = Post.objects.order_by('-date_posted')
    # queryset = Post.objects.order_by('date_posted')
    # queryset = Post.objects.order_by('author')
    # queryset = Post.objects.order_by('-author')

    template_name = 'blog/user_posts.html'  # в какой шаблон будет рендерить. по умолчанию будет/<app>/<model>_<viewtype>.html
    context_object_name = 'posts'  # название списка который будет рендериться в шаблон для форка.
    # ordering = ['-date_posted']
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')


class PostDetailView(DetailView):
    model = Post


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


def about(request):
    return render(request, 'blog/about.html', context={'title': 'About.'})

